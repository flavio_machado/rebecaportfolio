Lambedura dos Gatos

A lambedura excessiva dos gatos pode se transformar em compulsão. Confira

Todo gateiro sabe que esses peludos de longos bigodes adoram se lamber por horas. Apesar de ser uma atitude natural, os tutores devem ficar atentos a esse hábito, que, mesmo tão higiênico e instintivo, pode passar dos limites e se transformar em compulsão – nada agradável para o felino. A lambedura excessiva, geralmente, é uma resposta para a falta de estímulos e o estresse do pet. “Para minimizar esse problema, o ideal é oferecer encorajamentos, como brincadeiras, prateleiras pela casa para que o bichano possa se exercitar, além da mudança de postura do dono”, indica. Fora essas atitudes, que podem ser resolvidas em casa, alguns gatos precisam de acompanhamento médico e até tratamentos medicamentosos quando o ato passa da conta. “Quando o animal apresenta falhas no pelo ou demora muito se limpando, esse pode ser um sinal de que não está bem”, aponta Naila Fukimoto, da Cão Cidadão (SP).

Fonte: Revista Meu Pet

Aprenda como massagear seu cão


Assim como os humanos, cachorros podem sofrer picos de estresse e indisposição, e nesses casos a dica é massagear seu animal. Como?

Existem vários pontos de estímulo como atrás da cabeça, logo abaixo do osso do crânio. Posicione dois dedos nessa região e massageie, colocando pressão suficiente para que seja um estímulo relaxante. Se estiver forte demais, você perceberá sinais de sensibilidade.
Faça movimentos circulares, descendo pelo pescoço para promover um alongamento passivo da musculatura do animal.
Nas patas e na extremidade das orelhas, a massagem deve ser feita com pressão, de cima para baixo, em movimentos circulares.
Fonte: Revista Meu Pet