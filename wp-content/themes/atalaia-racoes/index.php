<?php get_header(); ?>

  <div class="jobs-e-rebeca">
    <div class="container">
      <div class="row">
        <!-- Posts padrões do wordpress -->
        <div class="jobs">
          <h2>
            <small>PORTFOLIO</small> <br>
            Quer conhecer meu trabalho?
          </h2>

          <div class="blog-container">
            <?php
            $news = get_posts(array(
              'post_type' => 'post',
              'posts_per_page' => -1,
              'orderby' => 'date',
              'order' => 'DESC',
              'post_status' => 'publish',
              'suppress_filters' => true 
            ));

            foreach ($news as $key => $post) {
              $thumb = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'novidades-thumb');
            ?>
              <a class="blog-post" href="<?= get_permalink($post->ID) ?>">
                <div class="imagem-thumb">
                  <?php if (isset($thumb[0])) { ?>
                    <img class="img-fluid" src="<?= $thumb[0]; ?>" alt="<?= $post->post_title ?>">
                  <?php } ?>

                </div>
                <div class="conteudo-post">
                  <h1><?= $post->post_title ?></h1>
                  <?= apply_filters('the_content', $post->post_excerpt) ?>
                </div>
              </a>
            <?php
            }
            ?>
          </div>
        </div>

        <!-- Sidebar / Widget (Promoçoes página da frente) -->
        <?php get_template_part('sidebar') ?>
      </div>
    </div>
  </div>

<?php get_footer(); ?>