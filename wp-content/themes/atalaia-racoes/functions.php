<?php
// defaults
require('functions/defaults/defaults.php');
// require('functions/defaults/admin-menu.php');

// register sidebar
require('functions/sidebars/sidebars.php');

// post types
require('functions/post-types/banners.php');
// require('functions/post-types/servicos.php');
// require('functions/post-types/parceiros.php');
// require('functions/post-types/nos-apoiamos.php');
// require('functions/post-types/nossas-unidades.php');
require('functions/post-types/rodape-dados.php');

function register_my_menus() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Principal' )
    )
  );
}
add_action( 'init', 'register_my_menus' );