<?php
get_header('interna');
global $post;
?>

<div class="jobs-e-rebeca-interno">
  <div class="container">
    <div class="row">
      <!-- Posts padrões do wordpress -->
      <div class="blog-interno">
        <div class="blog-content">
          <div class="titulo">
            <h1><?= $post->post_title ?></h1>
          </div>
          <div class="texto">
            <?= apply_filters('the_content', $post->post_content) ?>
          </div>
          
        </div>
      </div>
    </div>
  </div>
</div>

<?php get_footer(); ?>