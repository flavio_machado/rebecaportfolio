<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="theme-color" content="#0c4da2">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title><?= get_bloginfo('title') . ' - ' . get_bloginfo('description') ?></title>
  <meta name="description" content="Referência no estado de Sergipe com imensa variedade de produtos para animais de pequeno porte (cães, gatos, pássaros, pequenos roedores entre outros) com preços são justos e competitivos.
"/>
  <meta name="keywords" content="pet shop, banho e tosa, banho, tosa, consulta veterinária, vacina, disk ração, brinquedos, acessórios, coleiras, cães, gatos, pássaros, pequenos roedores">
  <link rel="stylesheet" href="<?php bloginfo('stylesheet_url') ?>" />
  <?php wp_head(); ?>
</head>
<body>


<div id="menu-legal" class="template-default-side-menu">
  <div class="menu-items">
    <div class="menu-header">
      <a href="#" role="button" class="menu-close menu-fechar">
        <i aria-hidden="true" class="fa fa-times-circle-o"></i>
      </a>
    </div>
    <span class="menu-caption">MENU PRINCIPAL</span>

    <?php
      if (!isset($menu)) {
          $menu = 'principal';
      }
      wp_nav_menu( array(
          'menu'              => $menu,
          'theme_location'    => $menu,
          'depth'             => 0,
          'container'         => 'ul',
          'container_class'   => '',
          'container_id'      => '',
          'menu_class'        => 'menu-items-list',
          // 'fallback_cb'       => 'wp_slide_menu::fallback',
          'walker'            => new wp_slide_menu())
          ); 

          ?>
      
  </div>
  <div class="template-default-side-menu-overlay menu-fechar"></div>
</div>


<div class="menu-branco">
  <div class="container">
    <div class="row">
      <div class="menu-conteudo">
        <!-- Chamar o "Diego's Menu -->
        <div class="menu-btn">
          <a id="menu-click" href="#">
            <i class="fa fa-bars" aria-hidden="true"></i> MENU
          </a>
        </div>
        <a href="<?= get_bloginfo('url') ?>" class="menu-marca">
          <picture>
            <source media="(min-width: 767px)" srcset="<?php bloginfo('template_url'); ?>/img/marca.png">
            <source class="responsive" media="(max-width: 767px)" srcset="<?php bloginfo('template_url'); ?>/img/marca-mobile.png"><img src="<?php bloginfo('template_url'); ?>/img/marca.png">
          </picture>
        </a>
        <div class="menu-social">
            <span class="fale">
              fale conosco
            </span>  
            <?php
              $services = get_posts(array(
                'post_type' => 'rodape',
                'posts_per_page' => 1,
                'orderby' => 'date',
                'order' => 'DESC',
                'post_status' => 'publish',
                'suppress_filters' => true 
              ));
              foreach ($services as $key => $service) {
                $thumb = wp_get_attachment_image_src(get_post_thumbnail_id($service->ID), 'thumbnail');
                ?>

                <span class="redes">
                   <?php if ($service->instagram != "") { ?>
                      <a href="<?= $service->instagram ?>">
                        <i class="fab fa-instagram" aria-hidden="true"></i>
                      </a>
                    <?php } ?>

                    <?php if ($service->facebook != "") { ?>
                      <a href="<?= $service->facebook ?>">
                        <i class="fab fa-youtube" aria-hidden="true"></i>
                      </a>
                    <?php } ?>

                    <?php if ($service->youtube != "") { ?>
                      <a href="<?= $service->youtube ?>">
                        <i class="fab fa-facebook-f" aria-hidden="true"></i>
                      </a>
                    <?php } ?>

                    <?php if ($service->twitter != "") { ?>
                      <a href="<?= $service->twitter ?>">
                        <i class="fab fa-twitter" aria-hidden="true"></i>
                      </a>
                    <?php } ?>
                </span>
            <?php
            }
            ?>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Custom post Superbanner 2 imagens (desktop e mobile) / link -->
<?php
$banners = get_posts(array(
  'post_type' => 'banners',
  'posts_per_page' => 9,
  'orderby' => 'date',
  'order' => 'DESC',
  'post_status' => 'publish',
  'suppress_filters' => true 
));
?>
<div id="superbanner" class="carousel slide" data-ride="carousel">
  <?php
  if (count($banners) > 1) {
  ?>
  <ol class="carousel-indicators">
    <?php foreach ($banners as $key => $banner) { ?>
    <li data-target="#superbanner" data-slide-to="<?= $key ?>" class="<?= $key === 0 ? 'active' : '' ?>"></li>
    <?php } ?>
  </ol>
  <?php
  }
  ?>

  <div class="carousel-inner">
    <?php
    foreach ($banners as $key => $banner) {
      $thumb_desktop = wp_get_attachment_image_src(
        get_post_thumbnail_id($banner->ID),
        'banner-thumb'
      );
      $thumb_mobile = wp_get_attachment_image_src(
        get_custom_thumbnails(
          $banner->ID,
          'banner-mobile',
          'banners'
        ),
        'promocoes-thumb'
      );
      $link = get_post_meta( $banner->ID, 'link', true );
    ?>
    <div class="carousel-item <?= $key === 0 ? 'active' : '' ?>">
      <a href="<?= isset($link) ? $link : '#' ?>">
        <picture>
          <source class="img-fluid" media="(min-width: 767px)" srcset="<?= $thumb_desktop[0]; ?>">
          <source class="img-fluid" media="(max-width: 767px)" srcset="<?= $thumb_mobile[0]; ?>">
          <img class="img-responsive" src="<?= $thumb_desktop[0]; ?>">
        </picture>
      </a>
    </div>
    <?php
    }
    ?>
  </div>
  <?php
  if (count($banners) > 1) {
  ?>
  <a class="carousel-control-prev" href="#superbanner" role="button" data-slide="prev">
    <i class="fas fa-chevron-left" aria-hidden="true"></i>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#superbanner" role="button" data-slide="next">
    <i class="fas fa-chevron-right" aria-hidden="true"></i>
    <span class="sr-only">Next</span>
  </a>
  <?php
  }
  ?>
</div>