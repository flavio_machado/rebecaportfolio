<div class="rebeca">
    <h2>
        Rebeca Meira
        <small>Quem sou, onde vivo, o que faço</small>
    </h2>
    <div class="conteudo-promocoes">
    <?php
    if ( is_active_sidebar( 'promocoes' ) ) {
        dynamic_sidebar( 'promocoes' );
    }
    ?>
    <?php
            $services = get_posts(array(
            'post_type' => 'rodape',
            'posts_per_page' => 1,
            'orderby' => 'date',
            'order' => 'DESC',
            'post_status' => 'publish',
            'suppress_filters' => true 
            ));
            foreach ($services as $key => $service) {
            $thumb = wp_get_attachment_image_src(get_post_thumbnail_id($service->ID), 'thumbnail');
            ?>

            <span class="redes-sidebar">
                <?php if ($service->instagram != "") { ?>
                    <a href="<?= $service->instagram ?>">
                    <i class="fab fa-instagram" aria-hidden="true"></i>
                    </a>
                <?php } ?>

                <?php if ($service->facebook != "") { ?>
                    <a href="<?= $service->facebook ?>">
                    <i class="fab fa-youtube" aria-hidden="true"></i>
                    </a>
                <?php } ?>

                <?php if ($service->youtube != "") { ?>
                    <a href="<?= $service->youtube ?>">
                    <i class="fab fa-facebook-f" aria-hidden="true"></i>
                    </a>
                <?php } ?>

                <?php if ($service->twitter != "") { ?>
                    <a href="<?= $service->twitter ?>">
                    <i class="fab fa-twitter" aria-hidden="true"></i>
                    </a>
                <?php } ?>
            </span>
        <?php
        }
        ?>
    </div>
</div>