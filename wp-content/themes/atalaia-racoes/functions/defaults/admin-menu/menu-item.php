<?php
add_action( 'admin_menu', 'site_config' );
function site_config() {
	add_options_page(
		'Configurações do site',
		'Configurações do site',
		'manage_options',
		'site-config',
		'site_config_options'
	);
}
function site_config_options() {
if (!current_user_can('manage_options')) {
	wp_die(__('Você não possui permissão para visualizar essa pagina.'));
}
$opt_prefix = 'site_';

$phone = get_option($opt_prefix . 'phone');
$facebook = get_option($opt_prefix . 'facebook');
$instagram = get_option($opt_prefix . 'instagram');
$youtube = get_option($opt_prefix . 'youtube');
$email = get_option($opt_prefix . 'email');

if (isset($_POST)) {
	save_site_config_data($_POST);
}
?>

<form action="" method="post">
	<h3>Digite abaixo as informações extra do rodapé do site</h3>
	<hr style="margin-bottom: 30px;">
	<label for="">
		Telefone:
		<input type="text" name="phone" value="<?= $phone ?>">
	</label>
	<br>
	<label for="">
		Email:
		<input type="email" name="email" value="<?= $email ?>">
	</label>
	<br>
	<label for="">
		Link do Facebook:
		<input type="text" name="facebook" value="<?= $facebook ?>">
	</label>
	<br>
	<label for="">
		Link do Instagram:
		<input type="text" name="instagram" value="<?= $instagram ?>">
	</label>
	<br>
	<label for="">
		Link do Youtube:
		<input type="text" name="youtube" value="<?= $youtube ?>">
	</label>
	<br>

	<input type="submit" value="Enviar">
</form>

<?php
}

function save_site_config_data($post) {
	$opt_prefix = 'site_';

	if (isset($post) && is_array($post) && count($post) > 0) {
		foreach ($post as $key => $value) {
			update_option($opt_prefix . $key, $value);
		}
	}
}