<?php
add_action( 'init', 'register_rodape_pt' );
function register_rodape_pt() {
	register_post_type(
        'rodape',
        array(
            'labels' => array(
                'name'               => _x( 'Rodapé', 'post type general name', 'your-plugin-textdomain' ),
                'singular_name'      => _x( 'Rodapé', 'post type singular name', 'your-plugin-textdomain' ),
                'menu_name'          => _x( 'Rodapé', 'admin menu', 'your-plugin-textdomain' ),
                'name_admin_bar'     => _x( 'Rodapé Dados', 'add new on admin bar', 'your-plugin-textdomain' ),
                'add_new'            => _x( 'Adicionar', 'Rodapé', 'your-plugin-textdomain' ),
                'add_new_item'       => __( 'Adicionar Rodapé', 'your-plugin-textdomain' ),
                'new_item'           => __( 'Novo Rodapé', 'your-plugin-textdomain' ),
                'edit_item'          => __( 'Editar Rodapé', 'your-plugin-textdomain' ),
                'view_item'          => __( 'Ver Rodape', 'your-plugin-textdomain' ),
                'all_items'          => __( 'Todos os Rodapés', 'your-plugin-textdomain' ),
                'search_items'       => __( 'Procurar Rodapé', 'your-plugin-textdomain' ),
                'parent_item_colon'  => __( 'Rodapé pai:', 'your-plugin-textdomain' ),
                'not_found'          => __( 'Nenhum Rodapé encontrado.', 'your-plugin-textdomain' ),
                'not_found_in_trash' => __( 'Nenhum Rodapé encontrado no lixo.', 'your-plugin-textdomain' )
            ),
            'description'        => __( 'Descrição.', 'your-plugin-textdomain' ),
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array( 'slug' => 'rodape' ),
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => false,
            'menu_position'      => null,
            'menu_icon'          => 'dashicons-editor-table',
            'supports'           => array( 'title' )
        )
    );
}

/** Custom Fields box **/
function rodape_add_meta_box() {
    add_meta_box(
        'rodape_text_data',
        __( 'Opções', 'myplugin_textdomain' ),
        'rodape_meta_box_callback',
        'rodape'
    );
}
add_action( 'add_meta_boxes', 'rodape_add_meta_box' );

function rodape_meta_box_callback( $post ) {
    // Add an nonce field so we can check for it later.
    wp_nonce_field(
        'custom_post_blocos_texto_meta_box',
        'custom_post_blocos_texto_meta_box_nonce'
    );

    $whatsapp = get_post_meta( $post->ID, 'whatsapp', true );
    $email = get_post_meta( $post->ID, 'email', true );
    $facebook = get_post_meta( $post->ID, 'facebook', true );
    $instagram = get_post_meta( $post->ID, 'instagram', true );
    $youtube = get_post_meta( $post->ID, 'youtube', true );
    $twitter = get_post_meta( $post->ID, 'twitter', true );
?>
    <style>
        #meta-box-fields-list label {
            display: block;
            font-size: 1.3em;
        }
        #meta-box-fields-list label > img {
            max-width: 100%;
            margin-bottom: 10px;
        }
        #meta-box-fields-list strong {
            font-size: 1.4em;
            margin-bottom: 7px;
            display: inline-block;
        }
    </style>
    <table id="meta-box-fields-list">
        <tbody>
            <tr>
                <td>
                    <strong>Telefone de Destaque</strong>
                    <label><input type="text" name="whatsapp" value="<?php echo $whatsapp; ?>"></label>
                </td>
            </tr>
            <tr>
                <td>
                    <strong>E-mail</strong>
                    <label><input type="text" name="email" value="<?php echo $email; ?>"></label>
                </td>
            </tr>
            <tr>
                <td>
                    <strong>Link do Facebook</strong>
                    <label><input type="text" name="facebook" value="<?php echo $facebook; ?>"></label>
                </td>
            </tr>
            <tr>
                <td>
                    <strong>Link do Instagram</strong>
                    <label><input type="text" name="instagram" value="<?php echo $instagram; ?>"></label>
                </td>
            </tr>
            <tr>
                <td>
                    <strong>Link do Youtube</strong>
                    <label><input type="text" name="youtube" value="<?php echo $youtube; ?>"></label>
                </td>
            </tr>
            <tr>
                <td>
                    <strong>Link Twitter</strong>
                    <label><input type="text" name="twitter" value="<?php echo $twitter; ?>"></label>
                </td>
            </tr>
        </tbody>
    </table>
<?php
}

function rodape_save_meta_box_data( $post_id ) {
    // Check if our nonce is set.
    if ( ! isset( $_POST['custom_post_blocos_texto_meta_box_nonce'] ) ) {
        return;
    }
    // Verify that the nonce is valid.
    if ( ! wp_verify_nonce( $_POST['custom_post_blocos_texto_meta_box_nonce'], 'custom_post_blocos_texto_meta_box' ) ) {
        return;
    }
    // If this is an autosave, our form has not been submitted, so we don't want to do anything.
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return;
    }
    
    // Make sure that it is set.
    if ( ! isset( $_POST['whatsapp'] ) ) {
        return;
    }
    if ( ! isset( $_POST['email'] ) ) {
        return;
    }
    if ( ! isset( $_POST['facebook'] ) ) {
        return;
    }
    if ( ! isset( $_POST['instagram'] ) ) {
        return;
    }
    if ( ! isset( $_POST['youtube'] ) ) {
        return;
    }
    if ( ! isset( $_POST['twitter'] ) ) {
        return;
    }

    // Update the meta field in the database.
    update_post_meta(
        $post_id,
        'whatsapp',
        sanitize_text_field($_POST['whatsapp'])
    );
    update_post_meta(
        $post_id,
        'email',
        sanitize_text_field($_POST['email'])
    );
    update_post_meta(
        $post_id,
        'facebook',
        sanitize_text_field($_POST['facebook'])
    );
    update_post_meta(
        $post_id,
        'instagram',
        sanitize_text_field($_POST['instagram'])
    );
    update_post_meta(
        $post_id,
        'youtube',
        sanitize_text_field($_POST['youtube'])
    );
    update_post_meta(
        $post_id,
        'twitter',
        sanitize_text_field($_POST['twitter'])
    );
}

add_action( 'save_post', 'rodape_save_meta_box_data' );