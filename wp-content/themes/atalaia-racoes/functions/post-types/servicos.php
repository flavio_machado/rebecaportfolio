<?php
add_action( 'init', 'register_servicos_pt' );
function register_servicos_pt() {
	register_post_type(
        'servicos',
        array(
            'labels' => array(
                'name'               => _x( 'Serviços', 'post type general name', 'your-plugin-textdomain' ),
                'singular_name'      => _x( 'Serviço', 'post type singular name', 'your-plugin-textdomain' ),
                'menu_name'          => _x( 'Serviços', 'admin menu', 'your-plugin-textdomain' ),
                'name_admin_bar'     => _x( 'Serviço', 'add new on admin bar', 'your-plugin-textdomain' ),
                'add_new'            => _x( 'Adicionar', 'Serviço', 'your-plugin-textdomain' ),
                'add_new_item'       => __( 'Adicionar Serviço', 'your-plugin-textdomain' ),
                'new_item'           => __( 'Novo Serviço', 'your-plugin-textdomain' ),
                'edit_item'          => __( 'Editar Serviço', 'your-plugin-textdomain' ),
                'view_item'          => __( 'Ver Serviço', 'your-plugin-textdomain' ),
                'all_items'          => __( 'Todos Serviços', 'your-plugin-textdomain' ),
                'search_items'       => __( 'Procurar Serviços', 'your-plugin-textdomain' ),
                'parent_item_colon'  => __( 'Serviços pai:', 'your-plugin-textdomain' ),
                'not_found'          => __( 'Nenhum Serviços encontrado.', 'your-plugin-textdomain' ),
                'not_found_in_trash' => __( 'Nenhum Serviços encontrado no lixo.', 'your-plugin-textdomain' )
            ),
            'description'        => __( 'Descrição.', 'your-plugin-textdomain' ),
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array( 'slug' => 'servico' ),
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => false,
            'menu_position'      => null,
            'menu_icon'          => 'dashicons-awards',
            'supports'           => array(
                'title',
                'editor',
                'thumbnail',
                'excerpt',
                'revisions',
            )
        )
    );
}