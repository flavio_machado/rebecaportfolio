<?php
add_action( 'init', 'register_banners_pt' );
function register_banners_pt() {
	register_post_type(
        'banners',
        array(
            'labels' => array(
                'name'               => _x( 'Banners', 'post type general name', 'your-plugin-textdomain' ),
                'singular_name'      => _x( 'Banner', 'post type singular name', 'your-plugin-textdomain' ),
                'menu_name'          => _x( 'Banners', 'admin menu', 'your-plugin-textdomain' ),
                'name_admin_bar'     => _x( 'Banner', 'add new on admin bar', 'your-plugin-textdomain' ),
                'add_new'            => _x( 'Adicionar', 'Banner', 'your-plugin-textdomain' ),
                'add_new_item'       => __( 'Adicionar Banner', 'your-plugin-textdomain' ),
                'new_item'           => __( 'Novo Banner', 'your-plugin-textdomain' ),
                'edit_item'          => __( 'Editar Banner', 'your-plugin-textdomain' ),
                'view_item'          => __( 'Ver Banner', 'your-plugin-textdomain' ),
                'all_items'          => __( 'Todos Banners', 'your-plugin-textdomain' ),
                'search_items'       => __( 'Procurar Banners', 'your-plugin-textdomain' ),
                'parent_item_colon'  => __( 'Banners pai:', 'your-plugin-textdomain' ),
                'not_found'          => __( 'Nenhum Banners encontrado.', 'your-plugin-textdomain' ),
                'not_found_in_trash' => __( 'Nenhum Banners encontrado no lixo.', 'your-plugin-textdomain' ),
                
            ),
            'description'        => __( 'Descrição.', 'your-plugin-textdomain' ),
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array( 'slug' => 'banner' ),
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => false,
            'menu_position'      => null,
            'menu_icon'          => 'dashicons-format-gallery',
            'supports'           => array( 'title', 'thumbnail', 'revisions' )
        )
    );
}

/** Custom Fields box **/
function banner_add_meta_box() {
    add_meta_box(
        'banner_text_data',
        __( 'Opções', 'myplugin_textdomain' ),
        'banner_meta_box_callback',
        'banners'
    );
}
add_action( 'add_meta_boxes', 'banner_add_meta_box' );

function banner_meta_box_callback( $post ) {
    // Add an nonce field so we can check for it later.
    wp_nonce_field(
        'custom_post_blocos_texto_meta_box',
        'custom_post_blocos_texto_meta_box_nonce'
    );

    $link = get_post_meta( $post->ID, 'link', true );
?>
    <style>
        #meta-box-fields-list label {
            display: block;
            font-size: 1.3em;
        }
        #meta-box-fields-list label > img {
            max-width: 100%;
            margin-bottom: 10px;
        }
        #meta-box-fields-list strong {
            font-size: 1.4em;
            margin-bottom: 7px;
            display: inline-block;
        }
    </style>
    <table id="meta-box-fields-list">
        <tbody>
            <tr>
                <td>
                    <strong>Link</strong>
                    <label><input type="text" name="link" value="<?php echo $link; ?>"></label>
                </td>
            </tr>
        </tbody>
    </table>
<?php
}

function banner_save_meta_box_data( $post_id ) {
    // Check if our nonce is set.
    if ( ! isset( $_POST['custom_post_blocos_texto_meta_box_nonce'] ) ) {
        return;
    }
    // Verify that the nonce is valid.
    if ( ! wp_verify_nonce( $_POST['custom_post_blocos_texto_meta_box_nonce'], 'custom_post_blocos_texto_meta_box' ) ) {
        return;
    }
    // If this is an autosave, our form has not been submitted, so we don't want to do anything.
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return;
    }
    
    // Make sure that it is set.
    if ( ! isset( $_POST['link'] ) ) {
        return;
    }

    // Update the meta field in the database.
    update_post_meta(
        $post_id,
        'link',
        sanitize_text_field($_POST['link'])
    );
}

add_action( 'save_post', 'banner_save_meta_box_data' );

// Add multiple post thumbnail
if (class_exists('MultiPostThumbnails')) {
    new MultiPostThumbnails(
        array(
            'label' => 'Imagem destacada mobile',
            'id' => 'banner-mobile',
            'post_type' => 'banners'
        )
    );
}