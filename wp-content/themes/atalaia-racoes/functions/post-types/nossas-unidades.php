<?php
add_action( 'init', 'register_unidades_pt' );
function register_unidades_pt() {
	register_post_type(
        'unidades',
        array(
            'labels' => array(
                'name'               => _x( 'Unidades', 'post type general name', 'your-plugin-textdomain' ),
                'singular_name'      => _x( 'Unidade', 'post type singular name', 'your-plugin-textdomain' ),
                'menu_name'          => _x( 'Unidades', 'admin menu', 'your-plugin-textdomain' ),
                'name_admin_bar'     => _x( 'Unidade', 'add new on admin bar', 'your-plugin-textdomain' ),
                'add_new'            => _x( 'Adicionar', 'Unidade', 'your-plugin-textdomain' ),
                'add_new_item'       => __( 'Adicionar Unidade', 'your-plugin-textdomain' ),
                'new_item'           => __( 'Nova Unidade', 'your-plugin-textdomain' ),
                'edit_item'          => __( 'Editar Unidade', 'your-plugin-textdomain' ),
                'view_item'          => __( 'Ver Unidade', 'your-plugin-textdomain' ),
                'all_items'          => __( 'Todas as Unidades', 'your-plugin-textdomain' ),
                'search_items'       => __( 'Procurar Unidades', 'your-plugin-textdomain' ),
                'parent_item_colon'  => __( 'Unidade pai:', 'your-plugin-textdomain' ),
                'not_found'          => __( 'Nenhuma Unidade encontrado.', 'your-plugin-textdomain' ),
                'not_found_in_trash' => __( 'Nenhuma Unidade encontrado no lixo.', 'your-plugin-textdomain' )
            ),
            'description'        => __( 'Descrição.', 'your-plugin-textdomain' ),
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array( 'slug' => 'unidade' ),
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => false,
            'menu_position'      => null,
            'menu_icon'          => 'dashicons-location-alt',
            'supports'           => array( 'title', 'revisions', 'editor' )
        )
    );
}

/** Custom Fields box **/
function unidades_add_meta_box() {
    add_meta_box(
        'unidades_text_data',
        __( 'Opções', 'myplugin_textdomain' ),
        'unidades_meta_box_callback',
        'unidades'
    );
}
add_action( 'add_meta_boxes', 'unidades_add_meta_box' );

function unidades_meta_box_callback( $post ) {
    // Add an nonce field so we can check for it later.
    wp_nonce_field(
        'custom_post_blocos_texto_meta_box',
        'custom_post_blocos_texto_meta_box_nonce'
    );

    $endereco = get_post_meta( $post->ID, 'endereco', true );
?>
    <style>
        #meta-box-fields-list label {
            display: block;
            font-size: 1.3em;
        }
        #meta-box-fields-list label > img {
            max-width: 100%;
            margin-bottom: 10px;
        }
        #meta-box-fields-list strong {
            font-size: 1.4em;
            margin-bottom: 7px;
            display: inline-block;
        }
    </style>
    <table id="meta-box-fields-list">
        <tbody>
            <tr>
                <td>
                    <strong>Endereço</strong>
                    <label><input type="text" name="endereco" value="<?php echo $endereco; ?>"></label>
                </td>
            </tr>
        </tbody>
    </table>
<?php
}

function unidades_save_meta_box_data( $post_id ) {
    // Check if our nonce is set.
    if ( ! isset( $_POST['custom_post_blocos_texto_meta_box_nonce'] ) ) {
        return;
    }
    // Verify that the nonce is valid.
    if ( ! wp_verify_nonce( $_POST['custom_post_blocos_texto_meta_box_nonce'], 'custom_post_blocos_texto_meta_box' ) ) {
        return;
    }
    // If this is an autosave, our form has not been submitted, so we don't want to do anything.
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return;
    }
    
    // Make sure that it is set.
    if ( ! isset( $_POST['endereco'] ) ) {
        return;
    }

    // Update the meta field in the database.
    update_post_meta(
        $post_id,
        'endereco',
        sanitize_text_field($_POST['endereco'])
    );
}

add_action( 'save_post', 'unidades_save_meta_box_data' );