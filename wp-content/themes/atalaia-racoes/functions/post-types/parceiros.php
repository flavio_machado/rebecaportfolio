<?php
add_action( 'init', 'register_parceiros_pt' );
function register_parceiros_pt() {
	register_post_type(
        'parceiros',
        array(
            'labels' => array(
                'name'               => _x( 'Parceiros', 'post type general name', 'your-plugin-textdomain' ),
                'singular_name'      => _x( 'Parceiro', 'post type singular name', 'your-plugin-textdomain' ),
                'menu_name'          => _x( 'Parceiros', 'admin menu', 'your-plugin-textdomain' ),
                'name_admin_bar'     => _x( 'Parceiro', 'add new on admin bar', 'your-plugin-textdomain' ),
                'add_new'            => _x( 'Adicionar', 'Parceiro', 'your-plugin-textdomain' ),
                'add_new_item'       => __( 'Adicionar Parceiro', 'your-plugin-textdomain' ),
                'new_item'           => __( 'Novo Parceiro', 'your-plugin-textdomain' ),
                'edit_item'          => __( 'Editar Parceiro', 'your-plugin-textdomain' ),
                'view_item'          => __( 'Ver Parceiro', 'your-plugin-textdomain' ),
                'all_items'          => __( 'Todos Parceiros', 'your-plugin-textdomain' ),
                'search_items'       => __( 'Procurar Parceiros', 'your-plugin-textdomain' ),
                'parent_item_colon'  => __( 'Parceiros pai:', 'your-plugin-textdomain' ),
                'not_found'          => __( 'Nenhum Parceiros encontrado.', 'your-plugin-textdomain' ),
                'not_found_in_trash' => __( 'Nenhum Parceiros encontrado no lixo.', 'your-plugin-textdomain' )
            ),
            'description'        => __( 'Descrição.', 'your-plugin-textdomain' ),
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array( 'slug' => 'parceiros' ),
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => false,
            'menu_position'      => null,
            'menu_icon'          => 'dashicons-groups',
            'supports'           => array(
                'title',
                'thumbnail',
                'revisions',
            )
        )
    );
}

/** Custom Fields box **/
function parceiros_add_meta_box() {
    add_meta_box(
        'parceiros_text_data',
        __( 'Opções', 'myplugin_textdomain' ),
        'parceiros_meta_box_callback',
        'parceiros'
    );
}
add_action( 'add_meta_boxes', 'parceiros_add_meta_box' );

function parceiros_meta_box_callback( $post ) {
    // Add an nonce field so we can check for it later.
    wp_nonce_field(
        'custom_post_blocos_texto_meta_box',
        'custom_post_blocos_texto_meta_box_nonce'
    );

    $link = get_post_meta( $post->ID, 'link', true );
?>
    <style>
        #meta-box-fields-list label {
            display: block;
            font-size: 1.3em;
        }
        #meta-box-fields-list label > img {
            max-width: 100%;
            margin-bottom: 10px;
        }
        #meta-box-fields-list strong {
            font-size: 1.4em;
            margin-bottom: 7px;
            display: inline-block;
        }
    </style>
    <table id="meta-box-fields-list">
        <tbody>
            <tr>
                <td>
                    <strong>Link</strong>
                    <label><input type="text" name="link" value="<?php echo $link; ?>"></label>
                </td>
            </tr>
        </tbody>
    </table>
<?php
}

function parceiros_save_meta_box_data( $post_id ) {
    // Check if our nonce is set.
    if ( ! isset( $_POST['custom_post_blocos_texto_meta_box_nonce'] ) ) {
        return;
    }
    // Verify that the nonce is valid.
    if ( ! wp_verify_nonce( $_POST['custom_post_blocos_texto_meta_box_nonce'], 'custom_post_blocos_texto_meta_box' ) ) {
        return;
    }
    // If this is an autosave, our form has not been submitted, so we don't want to do anything.
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return;
    }
    
    // Make sure that it is set.
    if ( ! isset( $_POST['link'] ) ) {
        return;
    }

    // Update the meta field in the database.
    update_post_meta(
        $post_id,
        'link',
        sanitize_text_field($_POST['link'])
    );
}

add_action( 'save_post', 'parceiros_save_meta_box_data' );