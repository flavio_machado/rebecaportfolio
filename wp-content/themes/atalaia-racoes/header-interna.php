<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="theme-color" content="#0c4da2">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title><?= get_bloginfo('title') . ' - ' . get_bloginfo('description') ?></title>
  <meta name="description" content="Referência no estado de Sergipe com imensa variedade de produtos para animais de pequeno porte (cães, gatos, pássaros, pequenos roedores entre outros) com preços são justos e competitivos.
"/>
  <meta name="keywords" content="pet shop, banho e tosa, banho, tosa, consulta veterinária, vacina, disk ração, brinquedos, acessórios, coleiras, cães, gatos, pássaros, pequenos roedores">
  <link rel="stylesheet" href="<?php bloginfo('stylesheet_url') ?>" />
  <?php wp_head(); ?>
</head>
<body>


<div id="menu-legal" class="template-default-side-menu">
  <div class="menu-items">
    <div class="menu-header">
      <h1>Atalaia<br>
        <small>Rações</small>
      </h1>
      <a href="#" role="button" class="menu-close menu-fechar">
        <i aria-hidden="true" class="fa fa-times-circle-o"></i>
      </a>
    </div>
    <span class="menu-caption">MENU PRINCIPAL</span>

    <?php
      if (!isset($menu)) {
          $menu = 'principal';
      }
      wp_nav_menu( array(
          'menu'              => $menu,
          'theme_location'    => $menu,
          'depth'             => 0,
          'container'         => 'ul',
          'container_class'   => '',
          'container_id'      => '',
          'menu_class'        => 'menu-items-list',
          // 'fallback_cb'       => 'wp_slide_menu::fallback',
          'walker'            => new wp_slide_menu())
          ); 

          ?>
      
  </div>
  <div class="template-default-side-menu-overlay menu-fechar"></div>
</div>


<div class="menu-branco">
  <div class="container">
    <div class="row">
      <div class="menu-conteudo">
        <!-- Chamar o "Diego's Menu -->
        <div class="menu-btn">
          <a id="menu-click" href="#">
            <i class="fa fa-bars" aria-hidden="true"></i> MENU
          </a>
        </div>
        <a href="<?= get_bloginfo('url') ?>" class="menu-marca">
          <picture>
            <source media="(min-width: 767px)" srcset="<?php bloginfo('template_url'); ?>/img/marca.png">
            <source class="responsive" media="(max-width: 767px)" srcset="<?php bloginfo('template_url'); ?>/img/marca-mobile.png"><img src="<?php bloginfo('template_url'); ?>/img/marca.png">
          </picture>
        </a>
        <div class="menu-social">
            <span class="fale">
              fale conosco
            </span>  
            <?php
              $services = get_posts(array(
                'post_type' => 'rodape',
                'posts_per_page' => 1,
                'orderby' => 'date',
                'order' => 'DESC',
                'post_status' => 'publish',
                'suppress_filters' => true 
              ));
              foreach ($services as $key => $service) {
                $thumb = wp_get_attachment_image_src(get_post_thumbnail_id($service->ID), 'thumbnail');
                ?>

                <span class="redes">
                   <?php if ($service->instagram != "") { ?>
                      <a href="<?= $service->instagram ?>">
                        <i class="fab fa-instagram" aria-hidden="true"></i>
                      </a>
                    <?php } ?>

                    <?php if ($service->facebook != "") { ?>
                      <a href="<?= $service->facebook ?>">
                        <i class="fab fa-youtube" aria-hidden="true"></i>
                      </a>
                    <?php } ?>

                    <?php if ($service->youtube != "") { ?>
                      <a href="<?= $service->youtube ?>">
                        <i class="fab fa-facebook-f" aria-hidden="true"></i>
                      </a>
                    <?php } ?>

                    <?php if ($service->twitter != "") { ?>
                      <a href="<?= $service->twitter ?>">
                        <i class="fab fa-twitter" aria-hidden="true"></i>
                      </a>
                    <?php } ?>
                </span>
            <?php
            }
            ?>
        </div>
      </div>
    </div>
  </div>
</div>