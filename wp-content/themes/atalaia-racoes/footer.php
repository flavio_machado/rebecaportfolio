<div class="rodape">
  <div class="container">
    <div class="row">

      <!-- Adicionar capacidade de adicionar mídia social / link / icone (font awesome mesmo) -->
      
      <?php
          $services = get_posts(array(
            'post_type' => 'rodape',
            'posts_per_page' => 1,
            'orderby' => 'date',
            'order' => 'DESC',
            'post_status' => 'publish',
            'suppress_filters' => true 
          ));
          foreach ($services as $key => $service) {
            $thumb = wp_get_attachment_image_src(get_post_thumbnail_id($service->ID), 'thumbnail');
            ?>

            <div class="midias">
              <img src="<?php bloginfo('template_url') ?>/img/marca-rodape.png" alt="">
              
              <div class="social">
                <h2>ME SEGUE AQUI!</h2>
                <?php if ($service->instagram != "") { ?>
                  <a href="<?= $service->instagram ?>">
                    <i class="fab fa-instagram" aria-hidden="true"></i>
                  </a>
                <?php } ?>

                <?php if ($service->facebook != "") { ?>
                  <a href="<?= $service->facebook ?>">
                    <i class="fab fa-facebook-f" aria-hidden="true"></i>
                  </a>
                <?php } ?>

                <?php if ($service->youtube != "") { ?>
                  <a href="<?= $service->youtube ?>">
                  <i class="fab fa-youtube" aria-hidden="true"></i>
                  </a>
                <?php } ?>

                <?php if ($service->twitter != "") { ?>
                  <a href="<?= $service->twitter ?>">
                    <i class="fab fa-twitter" aria-hidden="true"></i>
                  </a>
                <?php } ?>
                
              </div>
              <div class="email">
                <a href="mailto:<?= $service->email ?>">
                  <i class="fa fa-envelope-o" aria-hidden="true"></i> <?= $service->email ?>
                </a>
              </div>
              <small>
                Desenvolvido por: <br> 
                <a target="_blank" href="http://www.sensocriativo.com.br">Senso Criativo</a>
              </small>
          </div>
        </div>
      <?php
      }
      ?>
  </div>
</div>

<?php wp_footer(); ?>

<script src="<?php bloginfo('template_url'); ?>/bower_components/jquery/dist/jquery.slim.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/bower_components/popper.js/dist/umd/popper.js"></script>
<script src="<?php bloginfo('template_url'); ?>/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/slick.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/main.js"></script>
</body>
</html>